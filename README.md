# fb-chatbot
A Webapp2 based website used as a chatbot endpoint.

## Run & Update
``` sh
# Inside project directory.

# run
dev_appserver.py .

# update
appcfg.py update .
```

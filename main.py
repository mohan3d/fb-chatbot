#!/usr/bin/env python
import json
import logging
import urllib

import webapp2
from google.appengine.api import urlfetch

with open('client_secrets.json', 'r') as f:
    secrets = json.load(f)
    VERIFY_TOKEN = secrets.get('VERIFY_TOKEN')
    ACCESS_TOKEN = secrets.get('ACCESS_TOKEN')


def send_message(recipient_id, message_text):
    try:
        data = json.dumps({
            "recipient": {"id": recipient_id},
            "message": {"text": message_text}
        })

        params = urllib.urlencode({"access_token": ACCESS_TOKEN})
        headers = {"Content-Type": "application/json"}

        result = urlfetch.fetch(
            url='https://graph.facebook.com/v2.6/me/messages?{}'.format(params),
            payload=data,
            method=urlfetch.POST,
            headers=headers)
    except urlfetch.Error:
        logging.exception('Caught exception fetching url')


class MainHandler(webapp2.RequestHandler):
    def get(self):
        args = self.request.GET
        hub_mode = args.get('hub.mode')
        hub_challenge = args.get('hub.challenge')
        hub_verify_token = args.get("hub.verify_token")

        if hub_mode == 'subscribe' and hub_challenge:
            if not hub_verify_token == VERIFY_TOKEN:
                self.response.status(403)
                self.response.write("Mismatched verify token !")
            else:
                self.response.write(hub_challenge)
        else:
            self.response.write('Hello world!')

    def post(self):
        data_json = self.request.body
        data = json.loads(data_json)

        if data.get('object') == 'page':
            for entry in data["entry"]:
                for messaging_event in entry["messaging"]:
                    # someone sent us a message
                    if messaging_event.get("message"):
                        # the facebook ID of the person sending you the message
                        sender_id = messaging_event["sender"]["id"]

                        # the recipient's ID, which should be your page's facebook ID
                        # recipient_id = messaging_event["recipient"]["id"]

                        # the message's text
                        # message_text = messaging_event["message"]["text"]

                        send_message(sender_id, "Got You!")

        self.response.write('ok')


app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)
